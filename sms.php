<?php
    include('rc_authenticate.php');
    use RingCentral\SDK\Http\ApiException;

    $data = !empty($_POST)? $_POST : $_GET;

    function get_platform(){
        $rc_auth = new RC_Authentication();
        $platform = $rc_auth->get_platform();

        return $platform;
    }

    function read_extension_phone_number(){
        $platform = get_platform();
        $resp = $platform->get("/restapi/v1.0/account/~/extension/~/phone-number");
        $jsonObj = $resp->json();
        foreach ($resp->json()->records as $record){
            foreach ($record->features as $feature){
                if ($feature == "SmsSender"){
                    return send_sms($record->phoneNumber);
                }
            }
            exit("No phone number found with 'SmsSender' feature enabled.");
        }
    }
    function send_sms($fromNumber){
        global $data;
        $platform = get_platform();

        try {
            $resp = $platform->post('/account/~/extension/~/sms',
                array(
                    'from' => array ('phoneNumber' => $fromNumber),
                    'to' => array(
                        array('phoneNumber' => $data['to'])
                    ),
                    'text' => $data['message']
                ));
            print("SMS sent. Message status: " . $resp->json()->messageStatus . PHP_EOL);
        } catch (\RingCentral\SDK\Http\ApiException $e) {
        // exit("Message: Fallo" . $e->message . PHP_EOL);
            exit("Message: Fallo");
        }
    }
    
    function send_multiple_sms($fromNumber){
        global $data;
        $platform = get_platform();
        
        try {
            $resp = $platform->post('/account/~/extension/~/sms', $data);
            print("SMS sent. Message status: " . $resp->json()->messageStatus . PHP_EOL);
        } catch (\RingCentral\SDK\Http\ApiException $e) {
        // exit("Message: Fallo" . $e->message . PHP_EOL);
            exit("Message: Fallo");
        }
    }

    read_extension_phone_number();
